<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'id' => '2',
            'user_id' => '2',
            'status' => '1',
            'title' => 'noidungbaipost',
            'description' => 'mieu ta bai post',
        ]);
    }
}
