<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login','App\Http\Controllers\Controller@getauthentica');
Route::post('/login','App\Http\Controllers\Controller@authentica')->name('postLogin');

Route::get('post','App\Http\Controllers\PageController@index');
Route::get('user','App\Http\Controllers\PageController@user');
Route::prefix('action')->group( function () {
    Route::name('action.')->group( function () {
        Route::get('/edit/{id}', 'App\Http\Controllers\PageController@edit')->name('edit');
        Route::put('/update/{id}', 'App\Http\Controllers\PageController@update')->name('update');
        Route::get('/delete/{id}', 'App\Http\Controllers\PageController@destroy')->name('destroy');
        Route::get('/create', 'App\Http\Controllers\PageController@create')->name('create');
        Route::post('/create', 'App\Http\Controllers\PageController@store')->name('store');
        Route::get('laravel-send-email', 'App\Http\Controllers\EmailController@sendEMail')->name('email');

    });
});
Route::get('pdfview',array('as'=>'pdfview','uses'=>'App\Http\Controllers\BestInterviewQuestionController@pdfview'));
