@extends('layouts.app')


<div class="container">
    <a href="{{ route('pdfview',['download'=>'pdf']) }}">Download PDF</a>
    <table>

        <div class="row">
            <div class="col-md-4">No</div>
            <div class="col-md-4">Title</div>
            <div class="col-md-4">Description</div>
        </div>
        @foreach ($post as $key => $post)
            <div class="row">
                <div class="col-md-4">{{ ++$key }}</div>
                <div class="col-md-4">{{ $post->title }}</div>
                <div class="col-md-4">{{$post->description }}</div>
            </div>
        @endforeach
    </table>
</div>

