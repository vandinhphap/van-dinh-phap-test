@extends('layouts.app')
@section('title', 'Tao san pham moi')
@section('content')
    <h1>Tao san pham moi</h1>
    <h1>{{ (session('message') ? session('message') : " ") }}</h1>
    <div class="error">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <form method="post" action="{{ route('action.store') }}">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Name</label>
            <input type="text" name="name" class="form-control"  aria-describedby="emailHelp" placeholder="ENTER NAME">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Checkbox</label>
            <input type="text" id="checkbox" name="status">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Title</label>
            <input type="text" name="title" class="form-control"  aria-describedby="emailHelp" placeholder="ENTER TITLE">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Description</label>
            <input type="text" name="description" class="form-control"  aria-describedby="emailHelp" placeholder="ENTER DESCRIPTION">
        </div>
        <div class="form-group">
            <select name="user_id" class="form-control">
                @forelse($users as $users)
                    <option value="{{ $users->id }}">{{ $users->name }}</option>
                @empty
                    <option>No data</option>
                @endforelse
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Tao moi</button>
    </form>
@stop

